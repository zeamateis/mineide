package fr.zeamateis.mineide.main;

import java.io.IOException;
import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import fr.zeamateis.mineide.config.PropertyConfiguration;
import fr.zeamateis.mineide.forge.FilesMinecraftForge;
import fr.zeamateis.mineide.frame.ILoader;
import fr.zeamateis.mineide.gradle.GradleToolingAPI;
import fr.zeamateis.mineide.util.ThreadUtils;
import fr.zeamateis.slaty.main.Slaty;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

//TODO Add lang properties in all classes
public class MineIDE extends Application implements ILoader {
	public static final String APP_NAME = "MineIDE";
	private static final Logger LOGGER = LogManager.getFormatterLogger(APP_NAME);

	private static MineIDE instance;

	public static MineIDE getInstance() {
		return MineIDE.instance;
	}

	private static Stage ideStage;

	public static Stage getStage() {
		return ideStage;
	}

	private final PropertyConfiguration config;
	private final FilesMinecraftForge forgeMDK;
	private final GradleToolingAPI api;

	private FXMLLoader tutorialLoader;
	private FXMLLoader ideLoader;

	private Scene ideScene;

	public MineIDE() throws IOException {
		instance = this;
		config = new PropertyConfiguration();
		forgeMDK = new FilesMinecraftForge();
		api = new GradleToolingAPI();
		MineIDE.getLogger().info("Pre-Initialize...");
		this.preInitialize();
		MineIDE.getLogger().info("Pre-Initialized !");
	}

	@Override
	public void init() throws IOException {
		MineIDE.getLogger().info("Initialize...");
		this.initialize();
		MineIDE.getLogger().info("Initialized !");
	}

	@Override
	public void start(Stage primaryStage) throws IOException {
		ideStage = primaryStage;
		MineIDE.getLogger().info("Post-Initialize...");
		this.postInitialize(primaryStage);
		MineIDE.getLogger().info("Post-Initialized !");
	}

	@Override
	public void stop() {
		ThreadUtils.getService().shutdown();
		Platform.exit();
		System.exit(0);
	}

	@Override
	public void preInitialize() throws IOException {
		UserInformation.writeUserInfo("user.info");
		Slaty.addLanguage(Locale.FRANCE, Locale.ENGLISH, Locale.US);
		Slaty.load();
		this.ideLoader = new FXMLLoader(getClass().getResource("/fxml/mineide.fxml"));
		this.ideScene = new Scene(ideLoader.load(), 1280, 720);
	}

	@Override
	public void initialize() throws IOException {
		tutorialLoader = new FXMLLoader(getClass().getResource("/fxml/video_tutorial.fxml"));
	}

	@Override
	public void postInitialize(Stage primaryStage) throws IOException {
		ideStage.getIcons().add(new Image(Main.class.getResourceAsStream("/images/icon.png")));
		ideStage.setTitle(APP_NAME);
		ideStage.setScene(this.ideScene);
		ideStage.setFullScreen(getConfig().getBoolean("fullscreen"));
		ideStage.show();
	}

	public static Logger getLogger() {
		return LOGGER;
	}

	public Scene getTutorialScene() {
		try {
			return new Scene(tutorialLoader.load());
		}
		catch (IOException e) {
			getLogger().catching(e);
			return null;
		}
	}

	public Scene getIdeScene() {
		return this.ideScene;
	}

	public PropertyConfiguration getConfig() {
		return this.config;
	}

	public FilesMinecraftForge getForgeMDK() {
		return this.forgeMDK;
	}

	public GradleToolingAPI getGradleToolingAPI() {
		return this.api;
	}

}