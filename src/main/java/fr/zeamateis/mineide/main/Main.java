package fr.zeamateis.mineide.main;

import com.sun.javafx.application.LauncherImpl;

public class Main {
	public static void main(String[] args) {
		LauncherImpl.launchApplication(MineIDE.class, SplashLoader.class, args);
	}
}