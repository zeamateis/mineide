package fr.zeamateis.mineide.main;

import fr.zeamateis.mineide.util.OSHelper;
import javafx.animation.FadeTransition;
import javafx.application.Preloader;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.ProgressBar;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class SplashLoader extends Preloader {

	private Stage preloaderStage;
	private Pane splashLayout;
	private static final double SPLASH_WIDTH = 704.0D;
	private static final double SPLASH_HEIGHT = 250.0D;

	public SplashLoader() {
		// Constructor is called before everything.
		MineIDE.getLogger().debug("SplashLoader constructor called, thread: " + Thread.currentThread().getName());
	}

	@Override
	public void init() throws Exception {
		MineIDE.getLogger().debug("SplashLoader#init (could be used to initialize preloader view), thread: " + Thread.currentThread().getName());
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		MineIDE.getLogger().debug("SplashLoader#start (showing preloader stage), thread: " + Thread.currentThread().getName());
		this.preloaderStage = primaryStage;

		final ImageView splash = new ImageView(new Image(Main.class.getResourceAsStream("/images/banner.png")));

		ProgressBar loadProgressPhase = new ProgressBar(ProgressBar.INDETERMINATE_PROGRESS);
		loadProgressPhase.setPrefWidth(SPLASH_WIDTH);

		this.splashLayout = new VBox();
		this.splashLayout.getChildren().addAll(splash, loadProgressPhase);

		this.splashLayout.setStyle("-fx-padding: 5; " + "-fx-background-color: gainsboro; " + "-fx-border-width:2; " + "-fx-border-color: " + "linear-gradient(" + "to bottom, " + "MediumSeaGreen, "
				+ "derive(MediumSeaGreen, 50%)" + ");");
		this.splashLayout.setEffect(new DropShadow());

		final Scene splashScene = new Scene(this.splashLayout, Color.TRANSPARENT);
		final Rectangle2D bounds = Screen.getPrimary().getBounds();

		primaryStage.setScene(splashScene);
		primaryStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - SPLASH_WIDTH / 2);
		primaryStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - SPLASH_HEIGHT / 2);
		primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/images/icon.png")));
		primaryStage.setTitle(MineIDE.APP_NAME);

		primaryStage.initStyle(StageStyle.UNDECORATED);
		primaryStage.setAlwaysOnTop(true);
		primaryStage.show();
	}

	@Override
	public void handleApplicationNotification(PreloaderNotification info) {}

	@Override
	public void handleStateChangeNotification(StateChangeNotification info) {
		// Handle state change notifications.
		StateChangeNotification.Type type = info.getType();
		switch (type) {
			case BEFORE_LOAD:
				// Called after SplashLoader#start is called.
				OSHelper.generateDirs();
				MineIDE.getLogger().debug("BEFORE_LOAD");
				break;
			case BEFORE_INIT:
				// Called before MineIDE#init is called.
				MineIDE.getLogger().debug("BEFORE_INIT");

				break;
			case BEFORE_START:
				// Called after MineIDE#init and before MineIDE#start is called.
				MineIDE.getLogger().debug("BEFORE_START");
				if (preloaderStage.isShowing()) {
					// fade out, hide stage at the end of animation
					FadeTransition ft = new FadeTransition(Duration.millis(2000), preloaderStage.getScene().getRoot());
					ft.setFromValue(1.0);
					ft.setToValue(0.0);
					final Stage s = preloaderStage;
					EventHandler<ActionEvent> eh = event -> {
						s.hide();
					};
					ft.setOnFinished(eh);
					ft.play();
				} else {
					preloaderStage.hide();
				}
				break;
		}
	}
}
