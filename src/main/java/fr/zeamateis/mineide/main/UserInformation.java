package fr.zeamateis.mineide.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Map;
import java.util.Set;

import fr.zeamateis.mineide.util.OSHelper;

public class UserInformation {

	public static void writeUserInfo(String pathIn) {
		RuntimeMXBean runtimeBean = ManagementFactory.getRuntimeMXBean();
		BufferedWriter writer = null;
		File logFile = new File(String.format("%s/%s", OSHelper.LOGS_DIR, pathIn));

		Map<String, String> systemProperties = runtimeBean.getSystemProperties();
		Set<String> keys = systemProperties.keySet();

		if (!logFile.exists()) {
			try {
				writer = new BufferedWriter(new FileWriter(logFile));

				for (String key : keys) {
					String value = systemProperties.get(key);
					try {
						writer.write(String.format("[%s] = %s.\n", key, value));
					}
					catch (IOException e) {
						MineIDE.getLogger().catching(e);
					}
				}

			}
			catch (Exception e) {
				MineIDE.getLogger().catching(e);
			}
			finally {
				try {
					if (writer != null)
						writer.close();
				}
				catch (Exception e) {}
				MineIDE.getLogger().info("User.Info Generated Successfully ! {%s}", logFile.getAbsolutePath());
			}
		}
	}
}
