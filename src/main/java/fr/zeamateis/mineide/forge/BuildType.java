package fr.zeamateis.mineide.forge;

public enum BuildType {

	RECOMMENDED,
	LATEST;

	public static String getRecommended() {
		return RECOMMENDED.toString();
	}

	public static String getLatest() {
		return LATEST.toString();
	}
}
