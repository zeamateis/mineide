package fr.zeamateis.mineide.forge;

//TODO Add all version <= 1.10
public enum ForgeVersion {
	LATEST("1.12.2"),
	MC_1_12_2("1.12.2");

	private String version;

	private ForgeVersion(String version) {
		this.version = version;
	}

	public String getVersion() {
		return this.version;
	}

	public static String getLatestVersion() {
		return LATEST.getVersion();
	}
}
