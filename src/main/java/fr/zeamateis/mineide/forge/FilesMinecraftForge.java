package fr.zeamateis.mineide.forge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fr.zeamateis.mineide.exceptions.ConnectivityException;
import fr.zeamateis.mineide.main.MineIDE;

public class FilesMinecraftForge {

	private static final String INDEX_URL = "http://files.minecraftforge.net/maven/net/minecraftforge/forge/index.json";

	private String mdkURL;
	private boolean urlGenerated;

	public FilesMinecraftForge() {}

	public void generateMDKURL() {
		try {
			String readedIndex = readUrl(INDEX_URL);

			JsonParser parser = new JsonParser();
			JsonElement parsedIndex = parser.parse(readedIndex);
			JsonObject indexObject = parsedIndex.getAsJsonObject();

			JsonObject promosObj = indexObject.get("md").getAsJsonObject().get("promos").getAsJsonObject();

			MineIDE.getLogger().debug(promosObj.get(ForgeVersion.getLatestVersion()).getAsJsonObject().get(BuildType.getRecommended()));

			String promos = promosObj.get(ForgeVersion.getLatestVersion()).getAsJsonObject().get(BuildType.getRecommended()).getAsString();

			mdkURL = String.format("https://files.minecraftforge.net/maven/net/minecraftforge/forge/%s-%s/forge-%s-%s-mdk.zip", ForgeVersion.getLatestVersion(), promos,
					ForgeVersion.getLatestVersion(), promos);
		}
		catch (Exception ex) {
			MineIDE.getLogger().catching(ex);
			urlGenerated = false;
		}
		finally {
			urlGenerated = true;
		}
	}

	public String getMDKURL() {
		return mdkURL;
	}

	public boolean isMDKURLGenerated() {
		return urlGenerated;
	}

	private static String readUrl(String urlString) throws IOException {
		BufferedReader reader = null;
		try {
			URL url = new URL(urlString);
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
			StringBuffer buffer = new StringBuffer();
			int read;
			char[] chars = new char[1024];
			while ((read = reader.read(chars)) != -1)
				buffer.append(chars, 0, read);

			return buffer.toString();
		}
		catch (UnknownHostException ex) {
			throw new ConnectivityException(urlString);
		}
		finally {
			if (reader != null)
				reader.close();
		}
	}
}