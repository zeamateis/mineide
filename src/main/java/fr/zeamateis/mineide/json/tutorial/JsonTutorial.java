package fr.zeamateis.mineide.json.tutorial;

import java.util.ArrayList;

public class JsonTutorial {

	private ArrayList<TutorialObject> tutorials = new ArrayList<TutorialObject>();

	public ArrayList<TutorialObject> getTutorials() {
		return tutorials;
	}

	public static class TutorialObject {

		private String title;
		private String url;

		public String getTitle() {
			return title;
		}

		public String getURL() {
			return url;
		}
	}
}