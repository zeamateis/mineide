package fr.zeamateis.mineide.project.model.content.item;

import fr.zeamateis.mineide.project.data.DataManager;
import fr.zeamateis.mineide.project.data.DataTypeList;
import fr.zeamateis.mineide.project.model.base.BaseDataContent;

public class ItemStack extends BaseDataContent {
	public ItemStack() {
		super("item-stack");

		this.getData().addByte("stackSize", 0);
		this.getData().addShort("itemDamage", 0);
		this.getData().addValue("item", Item.DATA_TYPE, new Item());
		this.getData().addValue("nbt", DataTypeList.DATA, new DataManager());
	}

}
