package fr.zeamateis.mineide.project.model.base;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import fr.zeamateis.mineide.project.data.save.ISaveable;

public abstract class BaseContent implements ISaveable {
	private final String contentType;

	public BaseContent(final String contentType) {
		this.contentType = contentType;
	}

	@Override
	public JsonElement toJson() {
		JsonObject all = new JsonObject();

		all.addProperty("content-type", this.contentType);
		all.add("data", this.dataToJson());

		return all;
	}

	@Override
	public void fromJson(final JsonElement json) {
		if (!json.isJsonObject())
			throw new IllegalArgumentException("json is not an object");

		JsonObject all = json.getAsJsonObject();

		String type = all.get("content-type").getAsString();
		JsonElement data = all.get("data");

		if (type.equals(this.contentType)) {
			this.dataFromJson(data);
		}
	}

	protected abstract JsonElement dataToJson();

	protected abstract void dataFromJson(JsonElement json);

}
