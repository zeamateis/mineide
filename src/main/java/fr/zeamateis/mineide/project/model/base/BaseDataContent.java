package fr.zeamateis.mineide.project.model.base;

import com.google.gson.JsonElement;

import fr.zeamateis.mineide.project.data.DataManager;

public class BaseDataContent extends BaseContent {
	private final DataManager data;

	public BaseDataContent(final String contentType) {
		super(contentType);
		this.data = new DataManager();
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		else if (obj instanceof BaseDataContent) {
			BaseDataContent content = (BaseDataContent) obj;
			return this.data.equals(content.data);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	protected JsonElement dataToJson() {
		return this.data.toJson();
	}

	@Override
	protected void dataFromJson(final JsonElement json) {
		this.data.fromJson(json);
	}

	public final DataManager getData() {
		return this.data;
	}
}
