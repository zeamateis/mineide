package fr.zeamateis.mineide.project.model.content.structure;

import java.util.List;

import org.gradle.internal.impldep.com.google.common.collect.ImmutableList;

public class Project {
	// Project information
	private String name;
	private String desc;
	private String version;

	// List of mods
	private List<Mod> mods;

	// Minecraft information
	private String mcVersion;
	private String forgeVersion;
	private String mappingVersion;

	public ImmutableList<Mod> getMods() {
		return ImmutableList.copyOf(this.mods);
	}

	public String getName() {
		return this.name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDesc() {
		return this.desc;
	}

	public void setDesc(final String desc) {
		this.desc = desc;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(final String version) {
		this.version = version;
	}

	public String getMinecraftVersion() {
		return this.mcVersion;
	}

	public void setMinecraftVersion(final String mcVersion) {
		this.mcVersion = mcVersion;
	}

	public String getForgeVersion() {
		return this.forgeVersion;
	}

	public void setForgeVersion(final String forgeVersion) {
		this.forgeVersion = forgeVersion;
	}

	public String getMappingVersion() {
		return this.mappingVersion;
	}

	public void setMappingVersion(final String mappingVersion) {
		this.mappingVersion = mappingVersion;
	}
}
