package fr.zeamateis.mineide.project.model.content;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.gradle.internal.impldep.com.google.common.collect.Lists;
import org.gradle.internal.impldep.com.google.common.collect.Maps;
import org.gradle.internal.impldep.com.google.common.collect.Sets;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import fr.zeamateis.mineide.project.model.base.BaseContent;

public final class Translation extends BaseContent {
	public static final String EMPTY = "";

	private static Translation instance;

	private final Map<String, Map<String, String>> translations;

	public static Translation getInstance() {
		if (Translation.instance == null)
			Translation.instance = new Translation();
		return Translation.instance;
	}

	private Translation() {
		super("translation");
		this.translations = Maps.newHashMap();
		this.translations.put(Translation.EMPTY, Maps.newHashMap());
	}

	public void reset() {
		this.translations.clear();
		this.translations.put(Translation.EMPTY, Maps.newHashMap());
	}

	public List<String> getAllLanguages() {
		final Set<String> languages = Sets.newHashSet();
		this.translations.forEach((key, trans) -> languages.addAll(trans.keySet()));
		final List<String> list = Lists.newArrayList(languages);
		list.sort(String.CASE_INSENSITIVE_ORDER);
		return list;
	}

	public List<String> getAllKeys() {
		final Set<String> keys = this.translations.keySet();
		keys.remove(Translation.EMPTY);
		final List<String> list = Lists.newArrayList(keys);
		list.sort(String.CASE_INSENSITIVE_ORDER);
		return list;
	}

	public Map<String, String> getAllTranslations(final String key) {
		return Maps.newHashMap(this.getKeyTranslation(key));
	}

	public boolean addKey(final String key) {
		if (key == null || key.isEmpty())
			throw new IllegalArgumentException("Key not valid");

		if (this.translations.containsKey(key))
			return false;

		final Map<String, String> keyLang = Maps.newHashMap();
		this.getAllLanguages().forEach(lang -> keyLang.put(lang, Translation.EMPTY));

		this.translations.put(key, keyLang);
		return true;
	}

	public boolean addLanguage(final String lang) {
		if (lang == null || lang.isEmpty())
			throw new IllegalArgumentException("Lang not valid");

		if (this.getAllLanguages().contains(lang))
			return false;

		this.translations.forEach((key, trans) -> trans.put(lang, Translation.EMPTY));
		return true;
	}

	private Map<String, String> getKeyTranslation(final String key) {
		if (key == null || key.isEmpty())
			throw new IllegalArgumentException("Key not valid");

		if (!this.translations.keySet().contains(key))
			throw new IllegalArgumentException("Key not valid");

		return this.translations.get(key);
	}

	public boolean setTranslation(final String key, final String lang, final String value) {
		if (lang == null || lang.isEmpty())
			throw new IllegalArgumentException("Lang not valid");
		if (value == null)
			throw new IllegalArgumentException("Value not valid");

		final Map<String, String> trans = this.getKeyTranslation(key);

		if (!trans.keySet().contains(lang))
			throw new IllegalArgumentException("Lang not valid");

		if (trans.get(lang).equals(value))
			return false;

		return trans.replace(lang, trans.get(lang), value);
	}

	public String getTranslation(final String key, final String lang) {
		if (lang == null || lang.isEmpty())
			throw new IllegalArgumentException("Lang not valid");

		final Map<String, String> trans = this.getKeyTranslation(key);

		if (!trans.keySet().contains(lang))
			throw new IllegalArgumentException("Lang not valid");

		return trans.get(lang);
	}

	@Override
	protected JsonElement dataToJson() {
		final JsonArray data = new JsonArray();

		this.getAllKeys().forEach((key) -> {
			final JsonObject trans = new JsonObject();

			// Key
			trans.addProperty("key", key);

			// Translations
			final JsonArray translations = new JsonArray();

			final Map<String, String> values = this.getAllTranslations(key);

			values.keySet().forEach((lang) -> {
				final String translateValue = values.get(lang);
				if (translateValue != Translation.EMPTY) {
					final JsonObject transObj = new JsonObject();
					transObj.addProperty("lang", lang);
					transObj.addProperty("value", translateValue);

					translations.add(transObj);
				}
			});

			trans.add("translations", translations);
			data.add(trans);
		});
		return data;
	}

	@Override
	protected void dataFromJson(final JsonElement data) {
		if (!data.isJsonArray())
			throw new IllegalArgumentException("json is not an array");
		final JsonArray array = data.getAsJsonArray();

		array.forEach((trans) -> {
			if (trans.isJsonObject()) {
				final JsonObject transObj = trans.getAsJsonObject();
				final String key = transObj.get("key").getAsString();
				this.addKey(key);
				final JsonArray translations = transObj.get("translations").getAsJsonArray();

				translations.forEach((elem) -> {
					if (elem.isJsonObject()) {
						final JsonObject elemObj = elem.getAsJsonObject();
						final String lang = elemObj.get("lang").getAsString();
						this.addLanguage(lang);
						this.setTranslation(key, lang, elemObj.get("value").getAsString());
					}
				});
			}
		});
	}
}