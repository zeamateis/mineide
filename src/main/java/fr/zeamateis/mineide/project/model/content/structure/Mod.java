package fr.zeamateis.mineide.project.model.content.structure;

import java.util.List;

import org.gradle.internal.impldep.com.google.common.collect.ImmutableList;

public class Mod {
	// Mod information
	private String modId;
	private String modName;
	private String modDesc;
	private String modVersion;

	private List<String> authors;

	public ImmutableList<String> getAuthors() {
		return ImmutableList.copyOf(this.authors);
	}

	public String getModId() {
		return this.modId;
	}

	public void setModId(final String modId) {
		this.modId = modId;
	}

	public String getModName() {
		return this.modName;
	}

	public void setModName(final String modName) {
		this.modName = modName;
	}

	public String getModDescription() {
		return this.modDesc;
	}

	public void setModDescription(final String modDesc) {
		this.modDesc = modDesc;
	}

	public String getModVersion() {
		return this.modVersion;
	}

	public void setModVersion(final String modVersion) {
		this.modVersion = modVersion;
	}
}
