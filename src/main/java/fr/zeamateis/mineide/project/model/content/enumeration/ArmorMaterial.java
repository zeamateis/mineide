package fr.zeamateis.mineide.project.model.content.enumeration;

import fr.zeamateis.mineide.project.model.base.BaseDataContent;

public class ArmorMaterial extends BaseDataContent {

	public ArmorMaterial() {
		super("armor-material");
		this.getData().addString("name", "");
		this.getData().addString("textureName", "");
		this.getData().addString("soundOnEquip", "");
		this.getData().addFloat("toughness", 0);
		this.getData().addInteger("durability", 0);
		this.getData().addInteger("reduceDamageHelmet", 0);
		this.getData().addInteger("reduceDamageBody", 0);
		this.getData().addInteger("reduceDamageLeggins", 0);
		this.getData().addInteger("reduceDamageBoot", 0);
		this.getData().addInteger("enchantability", 0);
	}

	public String getName() {
		return this.getData().getString("name");
	}

	public void setName(final String name) {
		this.getData().setString("name", name);
	}

	public String getTextureName() {
		return this.getData().getString("textureName");
	}

	public void setTextureName(final String textureName) {
		this.getData().setString("textureName", textureName);
	}

	public int getDurability() {
		return this.getData().getInteger("durability");
	}

	public void setDurability(final int durability) {
		this.getData().setInteger("durability", durability);
	}

	public int getReduceDamageHelmet() {
		return this.getData().getInteger("reduceDamageHelmet");
	}

	public void setReduceDamageHelmet(final int reduceDamageHelmet) {
		this.getData().setInteger("reduceDamageHelmet", reduceDamageHelmet);
	}

	public int getReduceDamageBody() {
		return this.getData().getInteger("reduceDamageBody");
	}

	public void setReduceDamageBody(final int reduceDamageBody) {
		this.getData().setInteger("reduceDamageBody", reduceDamageBody);
	}

	public int getReduceDamageLeggins() {
		return this.getData().getInteger("reduceDamageLeggins");
	}

	public void setReduceDamageLeggins(final int reduceDamageLeggins) {
		this.getData().setInteger("reduceDamageLeggins", reduceDamageLeggins);
	}

	public int getReduceDamageBoot() {
		return this.getData().getInteger("reduceDamageBoot");
	}

	public void setReduceDamageBoot(final int reduceDamageBoot) {
		this.getData().setInteger("reduceDamageBoot", reduceDamageBoot);
	}

	public int getEnchantability() {
		return this.getData().getInteger("enchantability");
	}

	public void setEnchantability(final int enchantability) {
		this.getData().setInteger("enchantability", enchantability);
	}

	public String getSoundOnEquip() {
		return this.getData().getString("soundOnEquip");
	}

	public void setSoundOnEquip(final String soundOnEquip) {
		this.getData().setString("soundOnEquip", soundOnEquip);
	}

	public float getToughness() {
		return this.getData().getFloat("toughness");
	}

	public void setToughness(final float toughness) {
		this.getData().setFloat("toughness", toughness);
	}
}
