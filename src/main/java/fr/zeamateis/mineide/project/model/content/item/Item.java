package fr.zeamateis.mineide.project.model.content.item;

import com.google.gson.JsonElement;

import fr.zeamateis.mineide.project.data.AvancedDataType;
import fr.zeamateis.mineide.project.data.DataType;
import fr.zeamateis.mineide.project.model.base.BaseDataContent;

public class Item extends BaseDataContent {
	public static final DataType<Item> DATA_TYPE = new DataTypeItem();

	public Item() {
		super("item");
		this.getData().addString("unlocalizedName", "");
		this.getData().addString("registerName", "");
		this.getData().addString("creativeTab", "");
		this.getData().addShort("maxStackSize", 64);
	}

	public String getUnlocalizedName() {
		return this.getData().getString("unlocalizedName");
	}

	public void setUnlocalizedName(String unlocalizedName) {
		this.getData().setString("unlocalizedName", unlocalizedName);
	}

	public String getRegisterName() {
		return this.getData().getString("registerName");
	}

	public void setRegisterName(String registerName) {
		this.getData().setString("registerName", registerName);
	}

	public String getCreativeTab() {
		return this.getData().getString("creativeTab");
	}

	public void setCreativeTab(String creativeTab) {
		this.getData().setString("creativeTab", creativeTab);
	}

	public short getMaxStackSize() {
		return this.getData().getShort("maxStackSize");
	}

	public void setMaxStackSize(short maxStackSize) {
		this.getData().setShort("maxStackSize", maxStackSize);
	}

	private static class DataTypeItem extends AvancedDataType<Item> {

		@Override
		public String getDataType() {
			return "item";
		}

		@Override
		public JsonElement toJson(Item value) {
			return value.getData().toJson();
		}

		@Override
		public Item fromJson(JsonElement json) {
			Item item = new Item();
			item.getData().fromJson(json);
			return item;
		}

	}
}
