package fr.zeamateis.mineide.project.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.google.gson.JsonElement;

public abstract class DataType<T>
{

    public abstract T readString(String text);

    public abstract String writeString(T value);

    public abstract boolean checkParseType(String text);

    public abstract T readStream(DataInput data) throws IOException;

    public abstract void writeStream(DataOutput data, T value) throws IOException;

    public abstract String getDataType();

    public abstract JsonElement toJson(T value);

    public abstract T fromJson(JsonElement json);

    public boolean isValidType(final DataType<?> type)
    {
        return this.equals(type);
    }

    @SuppressWarnings("unchecked")
    public void writeStreamIn(final DataOutput stream, final Object value) throws IOException
    {
        this.writeStream(stream, (T) value);
    }

    @SuppressWarnings("unchecked")
    public JsonElement toJsonIn(final Object value)
    {
        return this.toJson((T) value);
    }

    @Override
    public String toString()
    {
        return "data type: " + this.getDataType();
    }

}
