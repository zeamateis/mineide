package fr.zeamateis.mineide.project.data.save;

import com.google.gson.JsonElement;

public interface ISaveable {
	JsonElement toJson();

	void fromJson(JsonElement json);

}
