package fr.zeamateis.mineide.project.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

public final class DataTypeList {
	public static final DataType<Boolean> BOOLEAN = new DataType<Boolean>() {

		@Override
		public Boolean readString(final String text) {
			return Boolean.parseBoolean(text);
		}

		@Override
		public String writeString(final Boolean value) {
			return value ? "true" : "false";
		}

		@Override
		public boolean checkParseType(final String text) {
			return text.equalsIgnoreCase("true") || text.equalsIgnoreCase("false");
		}

		@Override
		public Boolean readStream(final DataInput data) throws IOException {
			return data.readBoolean();
		}

		@Override
		public void writeStream(final DataOutput data, final Boolean value) throws IOException {
			data.writeBoolean(value);
		}

		@Override
		public String getDataType() {
			return "boolean";
		}

		@Override
		public JsonElement toJson(final Boolean value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Boolean fromJson(final JsonElement json) {
			return json.getAsBoolean();
		}
	};

	public static final DataType<Byte> BYTE = new DataType<Byte>() {

		@Override
		public Byte readString(final String text) {
			return Byte.parseByte(text);
		}

		@Override
		public String writeString(final Byte value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Byte readStream(final DataInput data) throws IOException {
			return data.readByte();
		}

		@Override
		public void writeStream(final DataOutput data, final Byte value) throws IOException {
			data.writeByte(value);
		}

		@Override
		public String getDataType() {
			return "byte";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.SHORT.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Byte value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Byte fromJson(final JsonElement json) {
			return json.getAsByte();
		}
	};

	public static final DataType<Short> SHORT = new DataType<Short>() {
		@Override
		public Short readString(final String text) {
			return Short.parseShort(text);
		}

		@Override
		public String writeString(final Short value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Short readStream(final DataInput data) throws IOException {
			return data.readShort();
		}

		@Override
		public void writeStream(final DataOutput data, final Short value) throws IOException {
			data.writeShort(value);
		}

		@Override
		public String getDataType() {
			return "short";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.INTEGER.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Short value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Short fromJson(final JsonElement json) {
			return json.getAsShort();
		}
	};

	public static final DataType<Integer> INTEGER = new DataType<Integer>() {
		@Override
		public Integer readString(final String text) {
			return Integer.parseInt(text);
		}

		@Override
		public String writeString(final Integer value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Integer readStream(final DataInput data) throws IOException {
			return data.readInt();
		}

		@Override
		public void writeStream(final DataOutput data, final Integer value) throws IOException {
			data.writeInt(value);
		}

		@Override
		public String getDataType() {
			return "integer";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.LONG.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Integer value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Integer fromJson(final JsonElement json) {
			return json.getAsInt();
		}
	};

	public static final DataType<Long> LONG = new DataType<Long>() {
		@Override
		public Long readString(final String text) {
			return Long.parseLong(text);
		}

		@Override
		public String writeString(final Long value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Long readStream(final DataInput data) throws IOException {
			return data.readLong();
		}

		@Override
		public void writeStream(final DataOutput data, final Long value) throws IOException {
			data.writeLong(value);
		}

		@Override
		public String getDataType() {
			return "long";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.FLOAT.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Long value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Long fromJson(final JsonElement json) {
			return json.getAsLong();
		}
	};

	public static final DataType<Float> FLOAT = new DataType<Float>() {
		@Override
		public Float readString(final String text) {
			return Float.parseFloat(text);
		}

		@Override
		public String writeString(final Float value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Float readStream(final DataInput data) throws IOException {
			return data.readFloat();
		}

		@Override
		public void writeStream(final DataOutput data, final Float value) throws IOException {
			data.writeFloat(value);
		}

		@Override
		public String getDataType() {
			return "float";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.DOUBLE.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Float value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Float fromJson(final JsonElement json) {
			return json.getAsFloat();
		}
	};

	public static final DataType<Double> DOUBLE = new DataType<Double>() {
		@Override
		public Double readString(final String text) {
			return Double.parseDouble(text);
		}

		@Override
		public String writeString(final Double value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			try {
				this.readString(text);
				return true;
			}
			catch (final Exception e) {
				return false;
			}
		}

		@Override
		public Double readStream(final DataInput data) throws IOException {
			return data.readDouble();
		}

		@Override
		public void writeStream(final DataOutput data, final Double value) throws IOException {
			data.writeDouble(value);
		}

		@Override
		public String getDataType() {
			return "double";
		}

		@Override
		public JsonElement toJson(final Double value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Double fromJson(final JsonElement json) {
			return json.getAsDouble();
		}
	};

	public static final DataType<Character> CHARACTER = new DataType<Character>() {
		@Override
		public Character readString(final String text) {
			return text.toCharArray()[0];
		}

		@Override
		public String writeString(final Character value) {
			return value.toString();
		}

		@Override
		public boolean checkParseType(final String text) {
			return text.length() == 1;
		}

		@Override
		public Character readStream(final DataInput data) throws IOException {
			return data.readChar();
		}

		@Override
		public void writeStream(final DataOutput data, final Character value) throws IOException {
			data.writeChar(value);
		}

		@Override
		public String getDataType() {
			return "character";
		}

		@Override
		public boolean isValidType(final DataType<?> type) {
			return super.isValidType(type) || DataTypeList.STRING.isValidType(type);
		}

		@Override
		public JsonElement toJson(final Character value) {
			return new JsonPrimitive(value);
		}

		@Override
		public Character fromJson(final JsonElement json) {
			return json.getAsCharacter();
		}
	};

	public static final DataType<String> STRING = new DataType<String>() {
		@Override
		public String readString(final String text) {
			return text;
		}

		@Override
		public String writeString(final String value) {
			return value;
		}

		@Override
		public boolean checkParseType(final String text) {
			return true;
		}

		@Override
		public String readStream(final DataInput data) throws IOException {
			return data.readUTF();
		}

		@Override
		public void writeStream(final DataOutput data, final String value) throws IOException {
			data.writeUTF(value);
		}

		@Override
		public String getDataType() {
			return "string";
		}

		@Override
		public JsonElement toJson(final String value) {
			return new JsonPrimitive(value);
		}

		@Override
		public String fromJson(final JsonElement json) {
			return json.getAsString();
		}
	};

	public static final DataType<DataManager> DATA = new AvancedDataType<DataManager>() {

		@Override
		public String getDataType() {
			return "data-manager";
		}

		@Override
		public JsonElement toJson(final DataManager value) {
			return value.toJson();
		}

		@Override
		public DataManager fromJson(final JsonElement json) {
			DataManager dt = new DataManager();
			dt.fromJson(json);
			return dt;
		}

	};

	/**
	 * Don't let anyone instantiate this class.
	 */
	private DataTypeList() {}
}
