package fr.zeamateis.mineide.project.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.gradle.internal.impldep.com.google.common.collect.Lists;
import org.gradle.internal.impldep.com.google.common.collect.Maps;
import org.gradle.internal.impldep.javax.annotation.Nullable;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import fr.zeamateis.mineide.project.data.save.ISaveable;

public final class DataManager implements ISaveable {
	private static Map<String, DataType<?>> typesValues = Maps.newHashMap();

	private final Map<String, DataType<?>> dataType;
	private final Map<String, Object> dataValue;

	static {
		registerDataType(DataTypeList.BOOLEAN);
		registerDataType(DataTypeList.BYTE);
		registerDataType(DataTypeList.SHORT);
		registerDataType(DataTypeList.INTEGER);
		registerDataType(DataTypeList.LONG);
		registerDataType(DataTypeList.FLOAT);
		registerDataType(DataTypeList.DOUBLE);
		registerDataType(DataTypeList.CHARACTER);
		registerDataType(DataTypeList.STRING);
		registerDataType(DataTypeList.DATA);
	}

	/**
	 * The comparator use by the sorter
	 */
	private static Comparator<String> sorter = (ob1, ob2) -> {
		return ob1.compareToIgnoreCase(ob2);
	};

	public static void registerDataType(final DataType<?> type) {
		if (!DataManager.typesValues.containsKey(type.getDataType()))
			DataManager.typesValues.put(type.getDataType(), type);
	}

	public DataManager() {
		this.dataType = Maps.newHashMap();
		this.dataValue = Maps.newHashMap();
	}

	/**
	 * Test if an entry is present
	 *
	 * @param key
	 *            The name of the entry
	 * @return true if contains the entry else false
	 * @throws IllegalArgumentException
	 *             If the key isn't valid
	 */
	public boolean hasEntry(final String key) {
		this.isValidKey(key);
		return this.dataValue.containsKey(key);
	}

	/**
	 *
	 * @return an array which contains all the register key in this data manager
	 */
	public String[] getAllKeys() {
		return this.dataValue.keySet().toArray(new String[this.dataValue.size()]);
	}

	/**
	 * Get the type of an entry by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The type of the entry or null
	 * @throws IllegalArgumentException
	 *             If the key isn't valid
	 */
	public @Nullable DataType<?> getType(final String key) {
		this.isValidKey(key);
		return this.dataType.getOrDefault(key, null);
	}

	/**
	 * Get the value of an entry by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry or null
	 * @throws IllegalArgumentException
	 *             If the key isn't valid
	 */
	public @Nullable Object getValue(final String key) {
		this.isValidKey(key);
		return this.dataValue.getOrDefault(key, null);
	}

	/**
	 * Replace a value
	 *
	 * @param key
	 *            The name of the entry
	 * @param oldValue
	 *            The old value
	 * @param newValue
	 *            The new value
	 * @return True if the value has changed
	 */
	private boolean replaceValue(final String key, final Object oldValue, final Object newValue) {
		// same value
		if (oldValue.equals(newValue))
			return false;
		this.dataValue.replace(key, newValue);
		return true;
	}

	/**
	 * Replace a value
	 *
	 * @param key
	 *            The name of the entry
	 * @param newValue
	 *            The new value
	 * @return True if the value has changed
	 */
	private boolean replaceValue(final String key, final Object newValue) {
		return this.replaceValue(key, this.getValue(key), newValue);
	}

	// *********************************************************************************************

	/**
	 * Add a new value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param type
	 *            The type of the entry
	 * @param value
	 *            The value of the entry
	 * @throws IllegalArgumentException
	 *             if DatatType is null
	 * @return True if value has been add to the system else false
	 */
	public boolean addValue(final String key, final DataType<?> type, final Object value) {
		// Key already exists
		if (this.hasEntry(key))
			return false;

		// Test valid DatatType
		if (type == null)
			this.throwArgumentException("Invalid DataType: null");

		// Add all the informations
		this.dataValue.put(key, value);
		this.dataType.put(key, type);
		return true;
	}

	/**
	 * Get a boolean value by giving its name
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	@SuppressWarnings("unchecked")
	public <T> T getValue(final DataType<T> type, final String key) {
		this.isValid(key, type);
		return (T) this.getValue(key);
	}

	/**
	 * Replace a value
	 *
	 * @param key
	 *            The name of the entry
	 * @param newValue
	 *            The new value
	 * @return True if the value has changed
	 */
	public <T> boolean replaceValue(final DataType<T> type, final String key, final T newValue) {
		this.isValid(key, type);
		return this.replaceValue(key, newValue);
	}

	/**
	 * Replace a value
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @param newValue
	 *            The new value
	 * @return True if the value has changed
	 */
	public <T> boolean replaceValue(final DataType<T> type, final String key, final T oldValue, final T newValue) {
		this.isValid(key, type);
		return this.replaceValue(key, oldValue, newValue);
	}

	/**
	 * Set a new value for an entry
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public <T> boolean setValue(final DataType<T> type, final String key, final T value) {
		return this.setInternalValue(type, key, value);
	}

	/**
	 * Set a new value for an entry
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	private boolean setInternalValue(final DataType<?> type, final String key, final Object value) {
		this.isValid(key, type);
		return this.replaceValue(key, value);
	}

	/**
	 * Set or add a new value for an entry
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	private boolean setOrAddInternalValue(final DataType<?> type, final String key, final Object value) {
		if (!this.addValue(key, type, value)) {
			this.isValid(key, type);
			return this.replaceValue(key, value);
		} else
			return true;

	}

	// *********************************************************************************************

	/**
	 * Add a new boolean value to the system
	 *
	 * @param type
	 *            The type of the value
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addBoolean(final String key, final boolean value) {
		return this.addValue(key, DataTypeList.BOOLEAN, value);
	}

	/**
	 * Add a new byte value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addByte(final String key, final byte value) {
		return this.addValue(key, DataTypeList.BYTE, value);
	}

	/**
	 * Add a new byte value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addByte(final String key, final int value) {
		if (value > Byte.MAX_VALUE || value < Byte.MIN_VALUE)
			this.invalidValue(key, value);
		return this.addByte(key, (byte) value);
	}

	/**
	 * Add a new short value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addShort(final String key, final short value) {
		return this.addValue(key, DataTypeList.SHORT, value);
	}

	/**
	 * Add a new short value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addShort(final String key, final int value) {
		if (value > Short.MAX_VALUE || value < Short.MIN_VALUE)
			this.invalidValue(key, value);
		return this.addShort(key, (short) value);
	}

	/**
	 * Add a new integer value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addInteger(final String key, final int value) {
		return this.addValue(key, DataTypeList.INTEGER, value);
	}

	/**
	 * Add a new long value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addLong(final String key, final long value) {
		return this.addValue(key, DataTypeList.LONG, value);
	}

	/**
	 * Add a new float value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addFloat(final String key, final float value) {
		return this.addValue(key, DataTypeList.FLOAT, value);
	}

	/**
	 * Add a new double value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addDouble(final String key, final double value) {
		return this.addValue(key, DataTypeList.DOUBLE, value);
	}

	/**
	 * Add a new character value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addCharacter(final String key, final char value) {
		return this.addValue(key, DataTypeList.CHARACTER, value);
	}

	/**
	 * Add a new string value to the system
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The value of the entry
	 * @return True if value has been add to the system else false
	 */
	public boolean addString(final String key, final String value) {
		return this.addValue(key, DataTypeList.STRING, value);
	}

	// *********************************************************************************************

	/**
	 * Get a boolean value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public boolean getBoolean(final String key) {
		return this.getValue(DataTypeList.BOOLEAN, key);
	}

	/**
	 * Get a byte value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public byte getByte(final String key) {
		return this.getValue(DataTypeList.BYTE, key);
	}

	/**
	 * Get a short value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public short getShort(final String key) {
		return this.getValue(DataTypeList.SHORT, key);
	}

	/**
	 * Get an integer value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public int getInteger(final String key) {
		return this.getValue(DataTypeList.INTEGER, key);
	}

	/**
	 * Get a long value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public long getLong(final String key) {
		return this.getValue(DataTypeList.LONG, key);
	}

	/**
	 * Get a float value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public float getFloat(final String key) {
		return this.getValue(DataTypeList.FLOAT, key);
	}

	/**
	 * Get a double value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public double getDouble(final String key) {
		return this.getValue(DataTypeList.DOUBLE, key);
	}

	/**
	 * Get a character value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public char getCharacter(final String key) {
		return this.getValue(DataTypeList.CHARACTER, key);
	}

	/**
	 * Get a string value by giving its name
	 *
	 * @param key
	 *            The name of the entry
	 * @return The value of the entry
	 * @throws IllegalArgumentException
	 *             If the entry type doesn't match
	 */
	public String getString(final String key) {
		return this.getValue(DataTypeList.STRING, key);
	}

	// *********************************************************************************************

	/**
	 * Set a new boolean value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setBoolean(final String key, final boolean value) {
		return this.setValue(DataTypeList.BOOLEAN, key, value);
	}

	/**
	 * Set a new byte value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setByte(final String key, final byte value) {
		return this.setValue(DataTypeList.BYTE, key, value);
	}

	/**
	 * Set a new byte value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setByte(final String key, final int value) {
		if (value > Byte.MAX_VALUE || value < Byte.MIN_VALUE)
			this.invalidValue(key, value);
		return this.setByte(key, (byte) value);
	}

	/**
	 * Set a new short value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setShort(final String key, final short value) {
		return this.setValue(DataTypeList.SHORT, key, value);
	}

	/**
	 * Set a new short value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setShort(final String key, final int value) {
		if (value > Short.MAX_VALUE || value < Short.MIN_VALUE)
			this.invalidValue(key, value);
		return this.setShort(key, (short) value);
	}

	/**
	 * Set a new integer value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setInteger(final String key, final int value) {
		return this.setValue(DataTypeList.INTEGER, key, value);
	}

	/**
	 * Set a new long value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setLong(final String key, final long value) {
		return this.setValue(DataTypeList.LONG, key, value);
	}

	/**
	 * Set a new float value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setFloat(final String key, final float value) {
		return this.setValue(DataTypeList.FLOAT, key, value);
	}

	/**
	 * Set a new double value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setDouble(final String key, final double value) {
		return this.setValue(DataTypeList.DOUBLE, key, value);
	}

	/**
	 * Set a new character value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setCharacter(final String key, final char value) {
		return this.setValue(DataTypeList.CHARACTER, key, value);
	}

	/**
	 * Set a new string value for an entry
	 *
	 * @param key
	 *            The name of the entry
	 * @param value
	 *            The new value for the entry
	 * @return True if value has changed else false
	 * @throws IllegalArgumentException
	 *             If the entry dones'nt exist or if the DataType does'nt match
	 */
	public boolean setString(final String key, final String value) {
		return this.setValue(DataTypeList.STRING, key, value);
	}

	// *********************************************************************************************

	/**
	 * Write all the data into the stream
	 *
	 * @param stream
	 *            The output stream
	 * @throws IOException
	 *             If there is a problem with the stream
	 */
	public void writeStream(final DataOutput stream) throws IOException {
		final List<String> list = this.sortKey();

		for (final String key : list)
			this.getType(key).writeStreamIn(stream, this.getValue(key));
	}

	/**
	 * Read all the data from the stream
	 *
	 * @param stream
	 *            The input stream
	 * @throws IOException
	 *             If there is a problem with the stream
	 */
	public void readStream(final DataInput stream) throws IOException {
		final List<String> list = this.sortKey();

		for (final String key : list)
			this.replaceValue(key, this.getType(key).readStream(stream));
	}

	// *********************************************************************************************

	@Override
	public JsonElement toJson() {
		final JsonArray all = new JsonArray();

		this.sortKey().forEach((key) -> {
			final JsonObject data = new JsonObject();
			final DataType<?> dt = this.getType(key);
			data.addProperty("key", key);
			data.addProperty("type", dt.getDataType());
			data.add("value", dt.toJsonIn(this.getValue(dt, key)));

			all.add(data);
		});

		return all;
	}

	@Override
	public void fromJson(final JsonElement json) {
		if (!json.isJsonArray())
			throw new IllegalArgumentException("json is not an array");
		final JsonArray array = json.getAsJsonArray();

		array.forEach((dt) -> {
			if (dt.isJsonObject()) {
				final JsonObject dtObj = dt.getAsJsonObject();
				final String key = dtObj.get("key").getAsString();
				final String type = dtObj.get("type").getAsString();
				final DataType<?> dataType = DataManager.typesValues.get(type);
				if (dataType != null) {
					final Object value = dataType.fromJson(dtObj.get("value"));
					this.setOrAddInternalValue(dataType, key, value);
				}
			}
		});
	}

	// *********************************************************************************************

	/**
	 * Test if the type of the entry match with the request one
	 *
	 * @param key
	 *            The name of the entry
	 * @param type
	 *            The type of the entry
	 * @throws IllegalArgumentException
	 *             If the key is not valid
	 */
	private void isValid(final String key, final DataType<?> type) {
		// Test the key
		this.isValidKey(key);

		// Check if the key type is the same as require
		if (!this.getType(key).isValidType(type))
			this.invalidType(key, type.getDataType());
	}

	/**
	 * Test if a key is valid
	 *
	 * @param key
	 *            The key name
	 * @throws IllegalArgumentException
	 *             If the key is not valid
	 */
	private void isValidKey(final String key) {
		if (key == null || key.isEmpty())
			this.throwArgumentException("Key null or empty");
	}

	private void invalidValue(final String key, final Object value) {
		this.throwArgumentException("Invalid value for '" + key + "'");
	}

	private void invalidType(final String key, final String type) {
		this.throwArgumentException("Type " + type + " is not valid for '" + key + "' (" + this.getType(key) + ")");
	}

	private void throwArgumentException(final String text) {
		throw new IllegalArgumentException(text);
	}

	// *********************************************************************************************

	/**
	 * Sort the key list by alphabetical order
	 *
	 * @return A sorted list of key
	 */
	private List<String> sortKey() {
		final List<String> list = Lists.newArrayList(this.dataValue.keySet());
		Collections.sort(list, DataManager.sorter);
		return list;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		else if (obj instanceof DataManager) {
			final DataManager dm = (DataManager) obj;
			if (this.dataType.equals(dm.dataType) && this.dataValue.equals(dm.dataValue))
				return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

}
