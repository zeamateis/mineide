package fr.zeamateis.mineide.project.data;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import com.google.gson.JsonParser;
import com.google.gson.JsonStreamParser;

public abstract class AvancedDataType<T> extends DataType<T> {
	private static final JsonParser PARSER = new JsonParser();

	@Override
	public T readString(final String text) {
		return this.fromJson(PARSER.parse(text));
	}

	@Override
	public String writeString(final T value) {
		return this.toJson(value).toString();
	}

	@Override
	public boolean checkParseType(final String text) {
		try {
			PARSER.parse(text);
			return true;
		}
		catch (final Exception e) {
			return false;
		}
	}

	@Override
	public T readStream(final DataInput data) throws IOException {
		return fromJson(new JsonStreamParser(data.readUTF()).next());
	}

	@Override
	public void writeStream(final DataOutput data, final T value) throws IOException {
		data.writeUTF(toJson(value).toString());
	}
}
