package fr.zeamateis.mineide.project.structure.item.base;

public enum ItemType {
	ROOT,
	CATEGORY,
	BLOCK,
	ITEM,
	ARMOR,
	ANIMAL,
	MONSTER;
}
