package fr.zeamateis.mineide.project.structure.item.base;

public interface IItem {
	String getName();

	@Override
	String toString();

}