package fr.zeamateis.mineide.project.structure.item;

import fr.zeamateis.mineide.project.structure.item.base.IItem;

public class CategoryItem implements IItem {

	private String name;

	public CategoryItem(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String toString() {
		return this.getName();
	}

}