package fr.zeamateis.mineide.gradle.tasks;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import fr.zeamateis.mineide.main.MineIDE;
import javafx.concurrent.Task;

public class DownloadZipTask extends Task<Void> {

	private String zipFileURL;
	private File outputZipFile;

	private FileOutputStream writer = null;
	private InputStream reader = null;

	public DownloadZipTask(String zipFileURLIn, File outputZipFileIn) {
		this.zipFileURL = zipFileURLIn;
		this.outputZipFile = outputZipFileIn;
	}

	@Override
	protected Void call() throws Exception {
		try {
			updateTitle(String.format("Downloading %s", zipFileURL));
			URL url = new URL(this.zipFileURL);
			url.openConnection();
			reader = url.openStream();

			writer = new FileOutputStream(this.outputZipFile);
			byte[] buffer = new byte[4096];
			int totalBytesRead = 0;
			int bytesRead = 0;
			while ((bytesRead = reader.read(buffer)) > 0) {
				writer.write(buffer, 0, bytesRead);
				buffer = new byte[4096];
				totalBytesRead += bytesRead;
				updateProgress(totalBytesRead, this.outputZipFile.length());
				updateMessage("" + totalBytesRead);
			}
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}

		return null;
	}

	@Override
	protected void failed() {
		super.failed();
		try {
			writer.close();
			reader.close();
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
	}

	@Override
	protected void succeeded() {
		super.succeeded();
		try {
			writer.close();
			reader.close();
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
	}

}