package fr.zeamateis.mineide.gradle.tasks;

import java.io.PrintStream;

import fr.zeamateis.mineide.frame.console.Console;
import fr.zeamateis.mineide.gradle.GradleToolingAPI;
import fr.zeamateis.mineide.util.ThreadUtils;
import javafx.concurrent.Task;
import javafx.scene.control.TextArea;

public class GradleIDETask extends Task<Void> {

	private final String task;
	private final GradleToolingAPI api;
	private final TextArea console;

	public GradleIDETask(GradleToolingAPI apiIn, TextArea consoleIn, String taskIn) {
		this.task = taskIn;
		this.api = apiIn;
		this.console = consoleIn;
		Console console = new Console(this.console);
		PrintStream ps = new PrintStream(console, true);
		this.api.getBuild().setStandardError(ps).setStandardOutput(ps);
	}

	@Override
	protected Void call() throws Exception {
		this.api.getBuild().forTasks(new String[] { this.task });
		this.api.run();
		return null;
	}

	@Override
	protected void failed() {
		ThreadUtils.getService().shutdown();
	}

	@Override
	protected void succeeded() {
		ThreadUtils.getService().shutdown();
	}

}