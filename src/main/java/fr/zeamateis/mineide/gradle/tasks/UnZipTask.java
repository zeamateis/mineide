package fr.zeamateis.mineide.gradle.tasks;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import fr.zeamateis.mineide.exceptions.UnzipTaskException;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.concurrent.Task;

public class UnZipTask extends Task<Void> {

	private File zipfile;
	private File folder;

	private ZipInputStream zis = null;
	private FileInputStream is = null;

	public UnZipTask(File zipfile, File folder) {
		this.zipfile = zipfile;
		this.folder = folder;
	}

	@Override
	protected Void call() throws Exception {
		is = new FileInputStream(zipfile.getCanonicalFile());
		FileChannel channel = is.getChannel();
		zis = new ZipInputStream(new BufferedInputStream(is));
		ZipEntry ze = null;

		updateTitle("Unzipping Task");

		while ((ze = zis.getNextEntry()) != null) {
			File f = new File(folder.getCanonicalPath(), ze.getName());
			if (ze.isDirectory()) {
				f.mkdirs();
				continue;
			}
			f.getParentFile().mkdirs();
			OutputStream fos = new BufferedOutputStream(new FileOutputStream(f));
			try {
				try {
					final byte[] buf = new byte[102400];
					int bytesRead;
					long nread = 0L;
					long length = zipfile.length();

					while (-1 != (bytesRead = zis.read(buf))) {
						fos.write(buf, 0, bytesRead);
						nread += bytesRead;
						updateProgress(channel.position(), length);
						updateMessage(nread + "/" + length);
					}
				}
				finally {
					fos.close();
				}
			}
			catch (final IOException ioe) {
				f.delete();
				MineIDE.getLogger().catching(ioe);
			}
		}
		return null;
	}

	@Override
	protected void failed() {
		try {
			zis.close();
			is.close();
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
		this.zipfile.delete();

		try {
			throw new UnzipTaskException(this.zipfile);
		}
		catch (UnzipTaskException e) {
			MineIDE.getLogger().catching(e);
		}
	}

	@Override
	protected void succeeded() {
		try {
			zis.close();
			is.close();
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
		this.zipfile.delete();
	}

}