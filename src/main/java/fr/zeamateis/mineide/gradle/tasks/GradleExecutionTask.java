package fr.zeamateis.mineide.gradle.tasks;

import java.io.PrintStream;

import fr.zeamateis.mineide.frame.console.Console;
import fr.zeamateis.mineide.frame.controller.GradleController;
import fr.zeamateis.mineide.frame.util.FxDialogs;
import fr.zeamateis.mineide.gradle.GradleToolingAPI;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.concurrent.Task;

public class GradleExecutionTask extends Task<Void> {

	private final GradleToolingAPI api;
	private final GradleController controller;

	public GradleExecutionTask(GradleController controllerIn) {
		this.api = MineIDE.getInstance().getGradleToolingAPI();
		this.controller = controllerIn;
		Console console = new Console(controllerIn.getTextArea());
		PrintStream ps = new PrintStream(console, true);
		this.api.getBuild().setStandardError(ps).setStandardOutput(ps);
	}

	@Override
	protected Void call() throws Exception {
		updateTitle("Executing Gradle Task");
		updateMessage("setupDecompWorkspace");
		this.api.getBuild().forTasks(new String[] { "setupDecompWorkspace" });
		this.api.run();
		return null;
	}

	@Override
	protected void failed() {
		this.api.close();
	}

	@Override
	protected void succeeded() {
		if (FxDialogs.showConfirm("Gradle setup finished with success !", "", FxDialogs.OK).equals(FxDialogs.OK)) {
			this.controller.enableComponents(false);
			MineIDE.getInstance().getConfig().setProperty("gradleSetupFinished", true);
		}
	}

}