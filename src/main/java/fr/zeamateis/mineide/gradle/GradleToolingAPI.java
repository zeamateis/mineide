package fr.zeamateis.mineide.gradle;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.gradle.tooling.BuildLauncher;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.model.GradleProject;
import org.gradle.tooling.model.GradleTask;
import org.gradle.util.GradleVersion;

import fr.zeamateis.mineide.util.OSHelper;

public class GradleToolingAPI {
	private static final File GRADLE_PROJECT_DIRECTORY = OSHelper.GRADLE_ENV;

	private GradleConnector connector;

	private final ProjectConnection connection;
	private final BuildLauncher build;

	public GradleToolingAPI() {
		connector = GradleConnector.newConnector();
		connector.forProjectDirectory(GRADLE_PROJECT_DIRECTORY);
		connection = connector.connect();
		build = connection.newBuild();// .setStandardError(System.err).setStandardOutput(System.out).setStandardInput(System.in);
	}

	public String getGradleVersion() {
		return GradleVersion.current().getVersion();
	}

	public List<String> getGradleTaskNames() {
		List<GradleTask> tasks = getGradleTasks();
		return tasks.stream().map(task -> task.getName()).collect(Collectors.toList());
	}

	public List<GradleTask> getGradleTasks() {
		List<GradleTask> tasks = new ArrayList<>();
		try {
			GradleProject project = connection.getModel(GradleProject.class);
			for (GradleTask task : project.getTasks()) {
				tasks.add(task);
			}
		}
		finally {
			this.close();
		}
		return tasks;
	}

	public boolean run() {
		try {
			build.run();
		}
		finally {
			this.close();
		}
		return true;
	}

	public void close() {
		this.connection.close();
	}

	public BuildLauncher getBuild() {
		return this.build;
	}

}