package fr.zeamateis.mineide.frame;

import java.io.IOException;

import javafx.stage.Stage;

public interface ILoader {

	void preInitialize() throws IOException;

	void initialize() throws IOException;

	void postInitialize(Stage primaryStage) throws IOException;
}
