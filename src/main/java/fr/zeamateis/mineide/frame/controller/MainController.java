package fr.zeamateis.mineide.frame.controller;

import java.awt.Desktop;
import java.net.URI;

import fr.zeamateis.mineide.main.Main;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainController {

	@FXML
	private MenuBar menuBar;

	@FXML
	private Menu menuFile;

	@FXML
	private MenuItem menuItemOpen;

	@FXML
	private MenuItem menuItemOpenRecent;

	@FXML
	private MenuItem menuItemSave;

	@FXML
	private MenuItem menuItemSaveAs;

	@FXML
	private MenuItem menuItemExportMod;

	@FXML
	private MenuItem menuItemClose;

	@FXML
	private Menu menuEdit;

	@FXML
	private MenuItem menuItemUndo;

	@FXML
	private MenuItem menuItemRedo;

	@FXML
	private Menu menuHelp;

	@FXML
	private MenuItem menuItemTutorials;

	@FXML
	private MenuItem menuItemAbout;

	@FXML
	private Menu menuSocial;

	@FXML
	private MenuItem menuItemTwitter;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private TextArea minecraftConsole;

	@FXML
	private TabPane tabPane;

	@FXML
	private GradleController gradleSetupController;

	@FXML
	private TaskToolController taskToolController;

	@FXML
	private ToolsController toolsController;

	@FXML
	private ProjectTreeViewController projectTreeViewController;

	@FXML
	private void initialize() {
		MineIDE.getLogger().debug("Initialize Main Controller...");

		assert menuBar != null : "fx:id=\"menuBar\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuFile != null : "fx:id=\"menuFile\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemOpen != null : "fx:id=\"menuItemOpen\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemOpenRecent != null : "fx:id=\"menuItemOpenRecent\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemSave != null : "fx:id=\"menuItemSave\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemSaveAs != null : "fx:id=\"menuItemSaveAs\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemExportMod != null : "fx:id=\"menuItemExportMod\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemClose != null : "fx:id=\"menuItemClose\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuEdit != null : "fx:id=\"menuEdit\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemUndo != null : "fx:id=\"menuItemUndo\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemRedo != null : "fx:id=\"menuItemRedo\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuHelp != null : "fx:id=\"menuHelp\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemTutorials != null : "fx:id=\"menuItemTutorials\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemAbout != null : "fx:id=\"menuItemAbout\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuSocial != null : "fx:id=\"menuSocial\" was not injected: check your FXML file 'mineide.fxml'.";
		assert menuItemTwitter != null : "fx:id=\"menuItemTwitter\" was not injected: check your FXML file 'mineide.fxml'.";
		assert progressBar != null : "fx:id=\"progressBar\" was not injected: check your FXML file 'mineide.fxml'.";
		assert minecraftConsole != null : "fx:id=\"minecraftConsole\" was not injected: check your FXML file 'mineide.fxml'.";
		assert tabPane != null : "fx:id=\"tabPane\" was not injected: check your FXML file 'mineide.fxml'.";

		this.initializeModifications();
		MineIDE.getLogger().debug("Main Controller Initialized ");
	}

	private void initializeModifications() {
		this.menuItemTwitter.setGraphic(new ImageView(Main.class.getResource("/images/twitter_16.png").toExternalForm()));
		this.gradleSetupController.enableComponents(!MineIDE.getInstance().getConfig().getBoolean("gradleSetupFinished"));
		this.taskToolController.setConsole(this.minecraftConsole);
		this.taskToolController.setTabPane(this.tabPane);
		this.taskToolController.setGradleController(gradleSetupController);
		this.toolsController.setProjectTreeViewController(projectTreeViewController);
	}

	@FXML
	void onAbout(ActionEvent event) {

	}

	@FXML
	void onClose(ActionEvent event) {

	}

	@FXML
	void onExportMod(ActionEvent event) {

	}

	@FXML
	void onFileAction(ActionEvent event) {

	}

	@FXML
	void onOpen(ActionEvent event) {

	}

	@FXML
	void onOpenRecent(ActionEvent event) {

	}

	@FXML
	void onRedo(ActionEvent event) {

	}

	@FXML
	void onSave(ActionEvent event) {}

	@FXML
	void onSaveAs(ActionEvent event) {

	}

	@FXML
	void onUndo(ActionEvent event) {

	}

	@FXML
	void onVideoTutorial(ActionEvent event) {
		final Stage dialog = new Stage();
		dialog.setTitle("Tutorials");
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(MineIDE.getStage());
		dialog.setScene(MineIDE.getInstance().getTutorialScene());
		dialog.show();
	}

	@FXML
	void onClickTwitter(ActionEvent event) {
		try {
			URI uri = new URI("https://twitter.com/ZeAmateis");
			Desktop dt = Desktop.getDesktop();
			dt.browse(uri);
		}
		catch (Exception ex) {}
	}

}
