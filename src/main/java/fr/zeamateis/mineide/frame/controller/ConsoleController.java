package fr.zeamateis.mineide.frame.controller;

import fr.zeamateis.mineide.frame.console.TextAreaAppender;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;

public class ConsoleController extends Tab {

	@FXML
	public TextArea consoleTextArea;

	@FXML
	private void initialize() {
		MineIDE.getLogger().debug("Initialize Console Controller...");
		assert consoleTextArea != null : "fx:id=\"consoleTextArea\" was not injected: check your FXML file 'console_area.fxml'.";
		TextAreaAppender.createAppender("JavaFXLogger", null, null);
		TextAreaAppender.setTextArea(consoleTextArea);
		MineIDE.getLogger().debug("Console Controller Initialized");
	}

}