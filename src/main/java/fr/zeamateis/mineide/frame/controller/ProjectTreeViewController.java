package fr.zeamateis.mineide.frame.controller;

import fr.zeamateis.mineide.project.structure.item.CategoryItem;
import fr.zeamateis.mineide.project.structure.item.base.IItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

public class ProjectTreeViewController {

	@FXML
	private TreeView<IItem> projectTreeView;

	private TreeItem<IItem> modRootItem = new TreeItem<IItem>(new CategoryItem("Mod"));
	private TreeItem<IItem> objectRootItem = new TreeItem<IItem>(new CategoryItem("Objects"));
	private TreeItem<IItem> entitiesRootItem = new TreeItem<IItem>(new CategoryItem("Entities"));

	private TreeItem<IItem> catBlocks = new TreeItem<IItem>(new CategoryItem("Blocks"));
	private TreeItem<IItem> catItems = new TreeItem<IItem>(new CategoryItem("Items"));
	private TreeItem<IItem> catArmors = new TreeItem<IItem>(new CategoryItem("Armors"));

	private TreeItem<IItem> catAnimal = new TreeItem<IItem>(new CategoryItem("Animal"));
	private TreeItem<IItem> catMob = new TreeItem<IItem>(new CategoryItem("Monster"));

	private TreeItem<IItem> selectedItem;

	@FXML
	void initialize() {
		assert projectTreeView != null : "fx:id=\"projectTreeView\" was not injected: check your FXML file 'project_tree_view.fxml'.";

		this.projectTreeView.setRoot(modRootItem);
		this.projectTreeView.setShowRoot(true);

		this.modRootItem.setExpanded(true);
		this.entitiesRootItem.setExpanded(true);
		this.objectRootItem.setExpanded(true);

		this.objectRootItem.getChildren().addAll(catBlocks, catItems, catArmors);
		this.entitiesRootItem.getChildren().addAll(catAnimal, catMob);
		this.modRootItem.getChildren().addAll(objectRootItem, entitiesRootItem);

		this.projectTreeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<IItem>>() {
			@Override
			public void changed(ObservableValue<? extends TreeItem<IItem>> observable, TreeItem<IItem> oldValue, TreeItem<IItem> newValue) {
				TreeItem<IItem> selectedItem = (TreeItem<IItem>) newValue;
				setSelectedItem(selectedItem);
				System.out.println(getSelectedItem().getValue().getName());
			}
		});

	}

	public TreeView<IItem> getProjectTreeView() {
		return projectTreeView;
	}

	public TreeItem<IItem> getModRootItem() {
		return modRootItem;
	}

	public TreeItem<IItem> getObjectRootItem() {
		return objectRootItem;
	}

	public TreeItem<IItem> getEntitiesRootItem() {
		return entitiesRootItem;
	}

	public TreeItem<IItem> getCatBlocks() {
		return catBlocks;
	}

	public TreeItem<IItem> getCatItems() {
		return catItems;
	}

	public TreeItem<IItem> getCatArmors() {
		return catArmors;
	}

	public TreeItem<IItem> getCatAnimal() {
		return catAnimal;
	}

	public TreeItem<IItem> getCatMob() {
		return catMob;
	}

	public TreeItem<IItem> getSelectedItem() {
		return selectedItem;
	}

	public void setSelectedItem(TreeItem<IItem> selectedItem) {
		this.selectedItem = selectedItem;
	}
}
