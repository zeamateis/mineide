package fr.zeamateis.mineide.frame.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

import fr.zeamateis.mineide.gradle.GradleToolingAPI;
import fr.zeamateis.mineide.gradle.tasks.GradleIDETask;
import fr.zeamateis.mineide.main.MineIDE;
import fr.zeamateis.mineide.util.OSHelper;
import fr.zeamateis.mineide.util.ThreadUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;

public class TaskToolController {

	@FXML
	private Button runClient;

	@FXML
	private Button runServer;

	@FXML
	private Button buildMod;

	@FXML
	private Button clearGradle;

	private TextArea console;
	private TabPane tabPane;

	private GradleToolingAPI api;

	private String[] gradleTask = new String[] { "build", "runClient", "runServer" };

	@FXML
	private GradleController gradleController;

	@FXML
	private void initialize() {
		MineIDE.getLogger().debug("Initialize Task Controller...");
		assert runClient != null : "fx:id=\"runClient\" was not injected: check your FXML file 'hbox_tasks.fxml'.";
		assert runServer != null : "fx:id=\"runServer\" was not injected: check your FXML file 'hbox_tasks.fxml'.";
		assert buildMod != null : "fx:id=\"buildMod\" was not injected: check your FXML file 'hbox_tasks.fxml'.";
		assert clearGradle != null : "fx:id=\"clearGradle\" was not injected: check your FXML file 'hbox_tasks.fxml'.";

		this.api = MineIDE.getInstance().getGradleToolingAPI();
		MineIDE.getLogger().debug("Task Controller Initialized");
	}

	public GradleController getGradleController() {
		return gradleController;
	}

	public void setGradleController(GradleController gradleController) {
		this.gradleController = gradleController;
	}

	@FXML
	void onBuildMod(ActionEvent event) {
		this.tabPane.getSelectionModel().select(1);
		ThreadUtils.getService().submit(new GradleIDETask(api, this.console, gradleTask[0]));
	}

	@FXML
	void onRunClient(ActionEvent event) {
		this.tabPane.getSelectionModel().select(1);
		ThreadUtils.getService().submit(new GradleIDETask(api, this.console, gradleTask[1]));
	}

	@FXML
	void onRunServer(ActionEvent event) {
		this.tabPane.getSelectionModel().select(1);
		ThreadUtils.getService().submit(new GradleIDETask(api, this.console, gradleTask[2]));
	}

	@FXML
	void onClearGradle(ActionEvent event) {
		try {
			MineIDE.getInstance().getConfig().setProperty("gradleSetupFinished", false);
			if (OSHelper.GRADLE_ENV.exists())
				FileUtils.deleteDirectory(OSHelper.GRADLE_ENV);

			Files.createDirectories(Paths.get(OSHelper.GRADLE_ENV.toURI()));
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
		finally {
			gradleController.enableComponents(true);
			gradleController.initialize();
		}
	}

	public void setConsole(TextArea consoleIn) {
		this.console = consoleIn;
	}

	public void setTabPane(TabPane pane) {
		this.tabPane = pane;
	}

}
