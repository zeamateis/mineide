package fr.zeamateis.mineide.frame.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.gson.Gson;

import fr.zeamateis.mineide.json.tutorial.JsonTutorial;
import fr.zeamateis.mineide.json.tutorial.JsonTutorial.TutorialObject;
import fr.zeamateis.mineide.main.Main;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

public class TutorialController {

	@FXML
	private WebView webView;

	@FXML
	private BorderPane borderPane;

	@FXML
	private ListView<TutorialObject> youtubeListView;

	@FXML
	private ListView<TutorialObject> textListView;

	private Gson gson = new Gson();

	@FXML
	private void initialize() {
		MineIDE.getLogger().debug("Initialize Tutorials Controller...");

		assert webView != null : "fx:id=\"webView\" was not injected: check your FXML file 'video_tutorial.fxml'.";
		assert borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'video_tutorial.fxml'.";
		assert youtubeListView != null : "fx:id=\"youtubeListView\" was not injected: check your FXML file 'video_tutorial.fxml'.";
		assert textListView != null : "fx:id=\"textListView\" was not injected: check your FXML file 'video_tutorial.fxml'.";
		final WebEngine webEngine = webView.getEngine();
		webEngine.load(getClass().getResource("/html/tutorials/index.html").toExternalForm());

		youtubeListView.setCellFactory(params -> new Cell());
		textListView.setCellFactory(params -> new Cell());

		BufferedReader reader = null;

		reader = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/json/tutorials/videos.json")));
		JsonTutorial videosObj = gson.fromJson(reader, JsonTutorial.class);
		ObservableList<TutorialObject> videosList = FXCollections.observableList(videosObj.getTutorials());
		youtubeListView.getItems().addAll(videosList);

		try {
			reader.close();
		}
		catch (IOException e1) {
			MineIDE.getLogger().catching(e1);
		}

		reader = new BufferedReader(new InputStreamReader(Main.class.getResourceAsStream("/json/tutorials/texts.json")));
		JsonTutorial textsObj = gson.fromJson(reader, JsonTutorial.class);
		ObservableList<TutorialObject> textsList = FXCollections.observableList(textsObj.getTutorials());
		textListView.getItems().addAll(textsList);

		try {
			reader.close();
		}
		catch (IOException e1) {
			MineIDE.getLogger().catching(e1);
		}

		youtubeListView.setOnMouseClicked(event -> {
			String embedVideo = String.format("%s", youtubeListView.getSelectionModel().getSelectedItem().getURL());
			webEngine.load(embedVideo);
		});

		textListView.setOnMouseClicked(event -> {
			String embedVideo = String.format("%s", textListView.getSelectionModel().getSelectedItem().getURL());
			webEngine.load(embedVideo);
		});

		MineIDE.getLogger().debug("Tutorials Controller Initialized");
	}

	private class Cell extends ListCell<TutorialObject> {
		private ImageView imageView = new ImageView();

		@Override
		protected void updateItem(TutorialObject item, boolean empty) {
			super.updateItem(item, empty);

			if (empty || item == null) {
				imageView.setImage(null);
				setGraphic(null);
				setText(null);
			} else {
				setText(item.getTitle());
			}
		}
	}
}
