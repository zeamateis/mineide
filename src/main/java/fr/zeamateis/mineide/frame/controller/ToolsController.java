package fr.zeamateis.mineide.frame.controller;

import fr.zeamateis.mineide.frame.util.ImageUtils;
import fr.zeamateis.mineide.main.Main;
import fr.zeamateis.mineide.main.MineIDE;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class ToolsController {

	@FXML
	private Button addBlock;
	private ImageView imageViewBlock = ImageUtils.sizedImageView(new ImageView(Main.class.getResource("/images/add_block.png").toExternalForm()), 25);

	@FXML
	private Button addEntity;
	private ImageView imageViewEntity = new ImageView(Main.class.getResource("/images/add_entity.png").toExternalForm());

	@FXML
	private Button addItem;
	private ImageView imageViewItem = new ImageView(Main.class.getResource("/images/add_item.png").toExternalForm());

	@FXML
	private Button addPotion;
	private ImageView imageViewPotion = ImageUtils.sizedImageView(new ImageView(Main.class.getResource("/images/add_potion.png").toExternalForm()), 25);

	@FXML
	private Button addArmor;
	private ImageView imageViewArmor = ImageUtils.sizedImageView(new ImageView(Main.class.getResource("/images/add_armor.png").toExternalForm()), 25);

	@FXML
	private ProjectTreeViewController projectTreeViewController;

	@FXML
	private void initialize() {
		MineIDE.getLogger().debug("Initialize Tools Controller...");

		assert addBlock != null : "fx:id=\"addBlock\" was not injected: check your FXML file 'hbox_tool.fxml'.";
		assert addEntity != null : "fx:id=\"addEntity\" was not injected: check your FXML file 'hbox_tool.fxml'.";
		assert addItem != null : "fx:id=\"addItem\" was not injected: check your FXML file 'hbox_tool.fxml'.";
		assert addPotion != null : "fx:id=\"addPotion\" was not injected: check your FXML file 'hbox_tool.fxml'.";
		assert addArmor != null : "fx:id=\"addArmor\" was not injected: check your FXML file 'hbox_tool.fxml'.";

		this.addBlock.setGraphic(ImageUtils.colorToGreyView(imageViewBlock, true));
		this.addEntity.setGraphic(ImageUtils.colorToGreyView(imageViewEntity, true));
		this.addItem.setGraphic(ImageUtils.colorToGreyView(imageViewItem, true));
		this.addPotion.setGraphic(ImageUtils.colorToGreyView(imageViewPotion, true));
		this.addArmor.setGraphic(ImageUtils.colorToGreyView(imageViewArmor, true));

		MineIDE.getLogger().debug("Tools Controller Initialized");
	}

	@FXML
	void onAddArmor(ActionEvent event) {}

	@FXML
	void onAddBlock(ActionEvent event) {}

	@FXML
	void onAddEntity(ActionEvent event) {}

	@FXML
	void onAddItem(ActionEvent event) {}

	@FXML
	void onAddPotion(ActionEvent event) {}

	@FXML
	void onHoverArmorButton(MouseEvent event) {
		this.addArmor.setGraphic(ImageUtils.colorToGreyView(imageViewArmor, false));
	}

	@FXML
	void onHoverBlockItem(MouseEvent event) {
		this.addBlock.setGraphic(ImageUtils.colorToGreyView(imageViewBlock, false));
	}

	@FXML
	void onHoverEntityButton(MouseEvent event) {
		this.addEntity.setGraphic(ImageUtils.colorToGreyView(imageViewEntity, false));
	}

	@FXML
	void onHoverItemButton(MouseEvent event) {
		this.addItem.setGraphic(ImageUtils.colorToGreyView(imageViewItem, false));
	}

	@FXML
	void onHoverPotionButton(MouseEvent event) {
		this.addPotion.setGraphic(ImageUtils.colorToGreyView(imageViewPotion, false));
	}

	@FXML
	void onExitArmorButton(MouseEvent event) {
		this.addArmor.setGraphic(ImageUtils.colorToGreyView(imageViewArmor, true));
	}

	@FXML
	void onExitBlockButton(MouseEvent event) {
		this.addBlock.setGraphic(ImageUtils.colorToGreyView(imageViewBlock, true));
	}

	@FXML
	void onExitEntityButton(MouseEvent event) {
		this.addEntity.setGraphic(ImageUtils.colorToGreyView(imageViewEntity, true));
	}

	@FXML
	void onExitItemButton(MouseEvent event) {
		this.addItem.setGraphic(ImageUtils.colorToGreyView(imageViewItem, true));
	}

	@FXML
	void onExitPotionButton(MouseEvent event) {
		this.addPotion.setGraphic(ImageUtils.colorToGreyView(imageViewPotion, true));
	}

	public ProjectTreeViewController getProjectTreeViewController() {
		return projectTreeViewController;
	}

	public void setProjectTreeViewController(ProjectTreeViewController projectTreeViewController) {
		this.projectTreeViewController = projectTreeViewController;
	}

}
