package fr.zeamateis.mineide.frame.controller;

import java.io.File;

import fr.zeamateis.mineide.gradle.tasks.DownloadZipTask;
import fr.zeamateis.mineide.gradle.tasks.GradleExecutionTask;
import fr.zeamateis.mineide.gradle.tasks.UnZipTask;
import fr.zeamateis.mineide.main.MineIDE;
import fr.zeamateis.mineide.util.OSHelper;
import fr.zeamateis.mineide.util.ThreadUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.text.Text;

public class GradleController {

	@FXML
	private AnchorPane backgroundPane;

	@FXML
	private BorderPane borderPane;

	@FXML
	private Label firstLabel;

	@FXML
	private Label secondLabel;

	@FXML
	private ProgressBar progressBar;

	@FXML
	private Text title;

	@FXML
	private TextArea textArea;

	@FXML
	void initialize() {
		MineIDE.getLogger().debug("Initialize Gradle Controller...");

		assert backgroundPane != null : "fx:id=\"backgroundPane\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert borderPane != null : "fx:id=\"borderPane\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert firstLabel != null : "fx:id=\"firstLabel\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert secondLabel != null : "fx:id=\"secondLabel\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert progressBar != null : "fx:id=\"progressBar\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert title != null : "fx:id=\"title\" was not injected: check your FXML file 'gradle_install.fxml'.";
		assert textArea != null : "fx:id=\"textArea\" was not injected: check your FXML file 'gradle_install.fxml'.";

		if (MineIDE.getInstance().getConfig().getBoolean("gradleSetupFinished").equals(false)) {
			MineIDE.getInstance().getForgeMDK().generateMDKURL();

			if (MineIDE.getInstance().getForgeMDK().isMDKURLGenerated()) {
				String forgeURL = MineIDE.getInstance().getForgeMDK().getMDKURL();

				DownloadZipTask downloadTask = new DownloadZipTask(forgeURL, new File(String.format("%s/%s", OSHelper.GRADLE_ENV, "forge-mdk.zip")));

				downloadTask.titleProperty().addListener((obs, oldMessage, newMessage) -> {
					firstLabel.setText(newMessage);
				});

				downloadTask.messageProperty().addListener((obs, oldMessage, newMessage) -> {
					secondLabel.setText(newMessage);
				});

				File zipFile = new File(String.format("%s/%s", OSHelper.GRADLE_ENV, "forge-mdk.zip"));
				UnZipTask unzipTask = new UnZipTask(zipFile, OSHelper.GRADLE_ENV);

				unzipTask.titleProperty().addListener((obs, oldMessage, newMessage) -> {
					firstLabel.setText(newMessage);
				});

				unzipTask.messageProperty().addListener((obs, oldMessage, newMessage) -> {
					secondLabel.setText(newMessage);
				});

				GradleExecutionTask gradleExecutionTask = new GradleExecutionTask(this);

				gradleExecutionTask.titleProperty().addListener((obs, oldMessage, newMessage) -> {
					firstLabel.setText(newMessage);
				});

				gradleExecutionTask.messageProperty().addListener((obs, oldMessage, newMessage) -> {
					secondLabel.setText(newMessage);
				});

				ThreadUtils.getService().submit(downloadTask);
				ThreadUtils.getService().submit(unzipTask);
				ThreadUtils.getService().submit(gradleExecutionTask);
				ThreadUtils.getService().shutdown();

			} else {
				this.secondLabel.setText("MDK URL Not Generated. Cancelling gradle setup");
			}
		}
		MineIDE.getLogger().debug("Gradle Controller Initialized");
	}

	public void enableComponents(boolean isEnabled) {
		this.backgroundPane.setVisible(isEnabled);
		this.borderPane.setVisible(isEnabled);
		this.firstLabel.setVisible(isEnabled);
		this.secondLabel.setVisible(isEnabled);
		this.progressBar.setVisible(isEnabled);
		this.title.setVisible(isEnabled);
		this.textArea.setVisible(isEnabled);

		this.backgroundPane.setDisable(!isEnabled);
		this.borderPane.setDisable(!isEnabled);
		this.firstLabel.setDisable(!isEnabled);
		this.secondLabel.setDisable(!isEnabled);
		this.progressBar.setDisable(!isEnabled);
		this.title.setDisable(!isEnabled);
		this.textArea.setDisable(!isEnabled);
	}

	public Label getFirstLabel() {
		return firstLabel;
	}

	public Label getSecondLabel() {
		return secondLabel;
	}

	public ProgressBar getProgressBar() {
		return progressBar;
	}

	public TextArea getTextArea() {
		return textArea;
	}

}
