package fr.zeamateis.mineide.frame.util;

import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.ImageView;

public class ImageUtils {

	public static ImageView colorToGreyView(ImageView imageViewIn, boolean monochromed) {

		ColorAdjust monochrome = new ColorAdjust();
		if (monochromed)
			monochrome.setSaturation(-1.0);
		else
			monochrome.setSaturation(0.0);

		imageViewIn.setEffect(monochrome);

		return imageViewIn;
	}

	public static ImageView sizedImageView(ImageView source, double sizeX, double sizeY) {
		source.setFitHeight(25);
		source.setFitWidth(25);
		source.setPreserveRatio(true);
		return source;
	}

	public static ImageView sizedImageView(ImageView source, double sizeXY) {
		return sizedImageView(source, sizeXY, sizeXY);
	}
}
