package fr.zeamateis.mineide.exceptions;

import java.io.File;

public class UnzipTaskException extends Exception {

	private static final long serialVersionUID = 8497409246364418344L;

	public UnzipTaskException(File zipFileIn) {
		super(String.format("Zip file '%s' seems to be corrupted, unable to Unzip it", zipFileIn.getName()));
	}

	public UnzipTaskException(String exceptionMessage, File zipFileIn) {
		super(String.format("%s - Errored File: %s", exceptionMessage, zipFileIn.getName()));
	}
}
