package fr.zeamateis.mineide.exceptions;

import java.net.UnknownHostException;

public class ConnectivityException extends UnknownHostException {

	private static final long serialVersionUID = 1L;

	public ConnectivityException(String urlIn) {
		super(String.format("Unable to reach %s, please check your internet connexion", urlIn));
	}
}
