package fr.zeamateis.mineide.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import fr.zeamateis.mineide.main.MineIDE;
import fr.zeamateis.mineide.util.OSHelper;

public class PropertyConfiguration {

	private Properties properties = new Properties();
	private OutputStream outputStream = null;
	private InputStream inputStream = null;

	private File propertiesFile = new File(String.format("%s/%s", OSHelper.CONFIG_DIR, "config.properties"));

	public PropertyConfiguration() {
		this.load();

		this.setProperty("fullscreen", false);
		this.setProperty("gradleSetupFinished", true);
	}

	private void load() {
		try (FileInputStream inputStream = new FileInputStream(propertiesFile)) {
			if (propertiesFile.exists()) {
				properties.load(inputStream);
			}
			outputStream = new FileOutputStream(propertiesFile);
		}
		catch (IOException ex) {
			MineIDE.getLogger().catching(ex);
		}
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				}
				catch (IOException e) {
					MineIDE.getLogger().catching(e);
				}
			} else if (outputStream != null) {
				try {
					outputStream.close();
				}
				catch (IOException e) {
					MineIDE.getLogger().catching(e);
				}
			}
		}
	}

	public String getString(String key) {
		return this.properties.getProperty(key);
	}

	public Boolean getBoolean(String key) {
		return Boolean.valueOf(this.properties.getProperty(key));
	}

	public boolean contain(String key) {
		return this.properties.containsKey(key);
	}

	public void setProperty(String key, Object value) {
		this.properties.setProperty(key, String.valueOf(value));
		this.save(null);
	}

	public void setProperty(String key, String value, String comment) {
		this.properties.setProperty(key, value);
		this.save(comment);
	}

	public void removeProperty(String key) {
		this.properties.remove(key);
		this.save(null);
	}

	private PropertyConfiguration save(String comments) {
		try {
			outputStream = new FileOutputStream(propertiesFile);
			properties.store(outputStream, comments);
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
		finally {
			if (outputStream != null) {
				try {
					outputStream.close();
				}
				catch (IOException e) {
					MineIDE.getLogger().catching(e);
				}
			}
		}
		return this;
	}

}