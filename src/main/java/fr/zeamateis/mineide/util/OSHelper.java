package fr.zeamateis.mineide.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import fr.zeamateis.mineide.main.MineIDE;

public class OSHelper {
	private static final String BASE_FOLDER = "MineIDE";
	private final static String osName = System.getProperty("os.name").toLowerCase();

	public static final File GRADLE_ENV = new File(getWorkingDirectory() + "/gradle");
	public static final File LOGS_DIR = new File(getWorkingDirectory() + "/logs");
	public static final File CONFIG_DIR = new File(getWorkingDirectory() + "/config");

	public static void generateDirs() {
		try {
			MineIDE.getLogger().debug("Creating all default directories...");
			Files.createDirectories(Paths.get(GRADLE_ENV.toURI()));
			Files.createDirectories(Paths.get(LOGS_DIR.toURI()));
			Files.createDirectories(Paths.get(CONFIG_DIR.toURI()));
		}
		catch (IOException e) {
			MineIDE.getLogger().catching(e);
		}
		finally {
			MineIDE.getLogger().debug("Created all default directories !");
		}
	}

	public static enum OS {
		WINDOWS,
		MACOS,
		SOLARIS,
		LINUX,
		UNKNOWN;
	}

	public static OS getPlatform() {
		if (osName.contains("win"))
			return OS.WINDOWS;
		if (osName.contains("mac"))
			return OS.MACOS;
		if (osName.contains("linux"))
			return OS.LINUX;
		if (osName.contains("unix"))
			return OS.LINUX;
		return OS.UNKNOWN;
	}

	public static boolean isWindows() {
		return osName.contains("win");
	}

	public static File getWorkingDirectory() {
		final String userHome = System.getProperty("user.home", ".");
		File workingDirectory;
		switch (getPlatform()) {
			case SOLARIS:
			case LINUX:
				workingDirectory = new File(userHome, BASE_FOLDER + "/");
				break;
			case WINDOWS:
				final String applicationData = System.getenv("APPDATA");
				final String folder = applicationData != null ? applicationData : userHome;

				workingDirectory = new File(folder, BASE_FOLDER + "/");
				break;
			case MACOS:
				workingDirectory = new File(userHome, "Library/Application Support/" + BASE_FOLDER + "/");
				break;
			default:
				workingDirectory = new File(userHome, BASE_FOLDER + "/");
		}

		return workingDirectory;
	}
}