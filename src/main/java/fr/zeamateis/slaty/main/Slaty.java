package fr.zeamateis.slaty.main;

import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author ZeAmateis
 */
public class Slaty {

	private static ResourceBundle resourceBundle;
	private static ResourceBundle defaultBundle;

	private static List<Locale> supportedLocale = new ArrayList<Locale>();
	private static Locale currentLocale;

	private static final String LANG_FORMAT = "%s_%s";
	private static ClassLoader localesLoader;

	private static final Logger LOGGER = LogManager.getFormatterLogger("Slaty");

	static {
		URL[] urls = { Slaty.class.getResource("/lang/") };
		localesLoader = new URLClassLoader(urls);
		currentLocale = Locale.getDefault();
		defaultBundle = ResourceBundle.getBundle(String.format(LANG_FORMAT, Locale.ENGLISH.getLanguage(), Locale.ENGLISH.getCountry()), Locale.ENGLISH, localesLoader);
	}

	public static void load() {
		if (supportedLocale.isEmpty())
			resourceBundle = ResourceBundle.getBundle(String.format(LANG_FORMAT, Locale.ENGLISH.getLanguage(), Locale.ENGLISH.getCountry()), Locale.ENGLISH, localesLoader);
		else {
			if (supportedLocale.contains(currentLocale)) {
				try {
					LOGGER.info("Setting language to %s", currentLocale);
					resourceBundle = ResourceBundle.getBundle(String.format(LANG_FORMAT, currentLocale.getLanguage(), currentLocale.getCountry()), currentLocale, localesLoader);
				}
				catch (MissingResourceException ex) {
					LOGGER.warn("Cannot find %s, setting default language (ENGLISH)", currentLocale);
					resourceBundle = defaultBundle;
				}
			} else {
				LOGGER.warn("%s, not supported yet, setting default language (ENGLISH)", currentLocale);
				resourceBundle = ResourceBundle.getBundle(String.format(LANG_FORMAT, Locale.ENGLISH.getLanguage(), Locale.ENGLISH.getCountry()), Locale.ENGLISH, localesLoader);
			}
		}
	}

	public static List<Locale> getSupportedLocale() {
		return supportedLocale;
	}

	public static Locale getCurrentLocale() {
		return currentLocale;
	}

	public static void addLanguage(Locale localeIn) {
		supportedLocale.add(localeIn);
	}

	public static void addLanguage(Locale... localesIn) {
		for (Locale locales : localesIn)
			supportedLocale.add(locales);
	}

	public static ResourceBundle getResourceBundle() {
		return resourceBundle;
	}

	public static String format(String keyIn, Object... args) {
		return String.format(resourceBundle.getString(keyIn), args);
	}

	public static String format(String keyIn) {
		return resourceBundle.getString(keyIn);
	}
}