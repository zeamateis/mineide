package fr.tests;

import fr.zeamateis.mineide.gradle.GradleToolingAPI;

public class GradleBuild {

	public static void main(String[] args) {
		GradleToolingAPI api = new GradleToolingAPI();

		api.getBuild().forTasks(new String[] { "runClient" });
		api.run();
	}

}
