package fr.tests.vpl;

// public class TreeManipulation extends Application {
//
// TreeView<ProjectItem> mainTree;
//
// TreeItem<ProjectItem> modRootItem = new TreeItem<ProjectItem>(new
// ProjectItem("ROOT-MOD", "Mod"));
// TreeItem<ProjectItem> objectRootItem = new TreeItem<ProjectItem>(new
// ProjectItem("ROOT-OBJECTS", "Objects"));
// TreeItem<ProjectItem> entitiesRootItem = new TreeItem<ProjectItem>(new
// ProjectItem("ROOT-ENTITIES", "Entities"));
//
// TreeItem<ProjectItem> selectedItem;
//
// @Override
// public void start(Stage primaryStage) throws Exception {
//
// TreeItem<ProjectItem> catBlocks = new TreeItem<ProjectItem>(new
// ProjectItem("BLOCKS", "Blocks"));
// TreeItem<ProjectItem> catItems = new TreeItem<ProjectItem>(new
// ProjectItem("ITEMS", "Items"));
// TreeItem<ProjectItem> catArmors = new TreeItem<ProjectItem>(new
// ProjectItem("ARMORS", "Armors"));
//
// TreeItem<ProjectItem> catAnimal = new TreeItem<ProjectItem>(new
// ProjectItem("ANIMAL", "Animal"));
// TreeItem<ProjectItem> catMob = new TreeItem<ProjectItem>(new
// ProjectItem("MONSTER", "Monster"));
//
// mainTree = new TreeView<ProjectItem>(modRootItem);
// mainTree.setShowRoot(true);
//
// modRootItem.setExpanded(true);
// entitiesRootItem.setExpanded(true);
// objectRootItem.setExpanded(true);
//
// objectRootItem.getChildren().addAll(catBlocks, catItems, catArmors);
// entitiesRootItem.getChildren().addAll(catAnimal, catMob);
// modRootItem.getChildren().addAll(objectRootItem, entitiesRootItem);
//
// mainTree.getSelectionModel().selectedItemProperty().addListener(new
// ChangeListener<TreeItem<ProjectItem>>() {
// @Override
// public void changed(ObservableValue<? extends TreeItem<ProjectItem>>
// observable, TreeItem<ProjectItem> oldValue, TreeItem<ProjectItem> newValue) {
// TreeItem<ProjectItem> selectedItem = (TreeItem<ProjectItem>) newValue;
// setSelectedItem(selectedItem);
// }
// });
//
// StackPane root = new StackPane();
// root.setPadding(new Insets(5));
// root.getChildren().add(mainTree);
//
// primaryStage.setTitle("Tree Manipulation");
// primaryStage.setScene(new Scene(root, 300, 250));
// primaryStage.show();
// }
//
// public TreeItem<ProjectItem> getSelectedItem() {
// return selectedItem;
// }
//
// public void setSelectedItem(TreeItem<ProjectItem> selectedItem) {
// this.selectedItem = selectedItem;
// }
//
// public static void main(String[] args) {
// launch(args);
// }
// }
