package fr.tests.vpl;

import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeType;
import javafx.stage.Stage;

/** Example of dragging anchors around to manipulate a line. */
public class LineManipulator extends Application {
	public static void main(String[] args) throws Exception {
		launch(args);
	}

	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("Line Manipulation");

		DoubleProperty startX = new SimpleDoubleProperty(100);
		DoubleProperty startY = new SimpleDoubleProperty(100);
		DoubleProperty endX = new SimpleDoubleProperty(300);
		DoubleProperty endY = new SimpleDoubleProperty(200);

		DoubleProperty startX1 = new SimpleDoubleProperty(50);
		DoubleProperty startY1 = new SimpleDoubleProperty(50);
		DoubleProperty endX1 = new SimpleDoubleProperty(150);
		DoubleProperty endY1 = new SimpleDoubleProperty(100);

		Anchor circle1 = new Anchor(Color.LIGHTSEAGREEN, startX, startY);
		Anchor circle2 = new Anchor(Color.TOMATO, endX, endY);
		Anchor circle3 = new Anchor(Color.CORNFLOWERBLUE, startX1, startY1);

		ParentAnchor parentCircle = new ParentAnchor(circle1, circle2, circle3);

		// Line line = new BoundLine(start, end);
		// Line line1 = new BoundLine(end, start1);
		// Line line2 = new BoundLine(start1, end1);

		Group group = new Group();

		group.getChildren().addAll(parentCircle.getChildren());

		stage.setScene(new Scene(group, 400, 400, Color.ALICEBLUE));
		stage.show();
	}

	class BoundLine extends Line {
		BoundLine(Anchor firstAnchor, Anchor secondAnchor) {
			startXProperty().bind(firstAnchor.getX());
			startYProperty().bind(firstAnchor.getY());
			endXProperty().bind(secondAnchor.getX());
			endYProperty().bind(secondAnchor.getY());
			setStrokeWidth(2);
			setStroke(Color.GRAY.deriveColor(0, 1, 1, 0.5));
			setStrokeLineCap(StrokeLineCap.BUTT);
			getStrokeDashArray().setAll(10.0, 5.0);
			setMouseTransparent(true);
		}
	}

	class ParentAnchor extends Anchor {

		private Anchor[] children;

		public ParentAnchor(Anchor... childs) {
			this.children = childs;
			for (Anchor child : childs) {
				this.setColor(child.getColor());
				setFill(child.getColor().deriveColor(1, 1, 1, 0.5));
				setStroke(child.getColor());
				this.setX(child.getX());
				this.setY(child.getY());
			}
		}

		public Anchor[] getChildren() {
			return children;
		}

	}

	class Anchor extends Circle {

		private DoubleProperty x, y;
		private Color color;

		public Anchor() {
			setStrokeWidth(2);
			setStrokeType(StrokeType.CENTERED);
			enableDrag();
		}

		public Anchor(Color colorIn, DoubleProperty x, DoubleProperty y) {
			super(x.get(), y.get(), 20);
			color = colorIn;
			setFill(color.deriveColor(1, 1, 1, 0.5));
			setStroke(color);
			setStrokeWidth(2);
			setStrokeType(StrokeType.CENTERED);

			this.setX(x);
			this.setY(y);

			x.bind(centerXProperty());
			y.bind(centerYProperty());

			enableDrag();
		}

		public DoubleProperty getX() {
			return x;
		}

		public void setX(DoubleProperty x) {
			this.x = x;
		}

		public DoubleProperty getY() {
			return y;
		}

		public void setY(DoubleProperty y) {
			this.y = y;
		}

		public Color getColor() {
			return color;
		}

		public void setColor(Color color) {
			this.color = color;
		}

		// make a node movable by dragging it around with the mouse.
		private void enableDrag() {
			final Delta dragDelta = new Delta();
			this.setOnMouseClicked(event -> {
				System.out.println(this.getParent());
			});
			setOnMousePressed(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					// record a delta distance for the drag and drop operation.
					dragDelta.x = getCenterX() - mouseEvent.getX();
					dragDelta.y = getCenterY() - mouseEvent.getY();
					getScene().setCursor(Cursor.MOVE);
				}
			});
			setOnMouseReleased(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					getScene().setCursor(Cursor.HAND);
				}
			});
			setOnMouseDragged(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					double newX = mouseEvent.getX() + dragDelta.x;
					if (newX > 0 && newX < getScene().getWidth()) {
						setCenterX(newX);
					}
					double newY = mouseEvent.getY() + dragDelta.y;
					if (newY > 0 && newY < getScene().getHeight()) {
						setCenterY(newY);
					}
				}
			});
			setOnMouseEntered(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					if (!mouseEvent.isPrimaryButtonDown()) {
						getScene().setCursor(Cursor.HAND);
					}
				}
			});
			setOnMouseExited(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent mouseEvent) {
					if (!mouseEvent.isPrimaryButtonDown()) {
						getScene().setCursor(Cursor.DEFAULT);
					}
				}
			});
		}

		// records relative x and y co-ordinates.
		private class Delta {
			double x, y;
		}
	}
}